## Accounts

Granting operator access to rucio can be acheived (at least) two ways:
 1. Grant access to an existing account
 1. Add a new user in rucio

See also: rucio docs for [`rucio-admin
account`](https://rucio.readthedocs.io/en/latest/man/rucio-admin.html#account)
sub-command.

### Granting Access To An Existing Account
Multple identities can be associated with existing accounts.  An obvious use case would be
to provide administrator access to a number of operators to provide e.g., time-zone redundancy.

To associate an operator's X509 identity with the root account, for example:
```
(gwrucio) $ rucio-admin identity add --account root --type X509 \
    --id "/DC=org/DC=XXXXXXX/C=XX/O=XXXX/CN=Albert Einstein albert.einstein@ligo.org" \
    --email albert.einstein@ligo.org
Added new identity to account: /DC=org/DC=XXXXXXX/C=XX/O=XXXX/CN=Albert Einstein albert.einstein@ligo.org albert.einstein@ligo.org-root
```
This example assumes the current operator already has access to the root account.  In this case,
we added Albert's X509 identity to the root account using the default root
identity on a newly-configured server (requires login access to the rucio server).

Once added, Albert has access to the root account and can proceed to add additional identities
for other operators, as well as creating new users as might be desired for site administrators
or even data production processes.

### Adding A New User Account
Add a new user account using `rucio-admin account add`:
```
(gwrucio) $ rucio-admin account add albert-einstein --email albert.einstein@ligo.org
Added new account: albert-einstein
```
Note that account names must follow the pattern `^[a-z0-9-_]{1,30}$`.  Then, as above, associate
the user's X509 identity with that account:
```
(gwrucio) $ rucio-admin identity add --account albert-einstein --type X509 \
    --id "/DC=org/DC=XXXXXXX/C=XX/O=XXXX/CN=Albert Einstein albert.einstein@ligo.org" \
    --email albert.einstein@ligo.org
```
By default, new accounts are of type `USER`, with limited privileges.

And remember to set `X509_USER_PROXY`.  E.g.,:
```
export X509_USER_PROXY=$(grid-proxy-info -path)
```


## Scopes
As described elsewhere, scopes in gwrucio are used to describe the observing
epoch (e.g., ER9, O2, postO2, etc) data is aquired from.  Scopes must be created
*prior* to any data registered to that scope.

To create the ER9 scope:
```
$ rucio-admin scope add --account root --scope ER9
Added new scope to account: ER9-root
```
So scopes are associated with user accounts.  Here, we are using the rucio root
account.


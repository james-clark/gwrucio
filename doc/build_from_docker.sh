#!/bin/bash
docker run -u $(id -u):$(id -g) \
  -w $PWD \
  -v /home:/home \
  jclarkastro/gwrucio-doc:latest \
  ./build.sh

#!/bin/bash

cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))"

# convert README to index.rst for sphinx
pandoc --from=gfm --to=rst --output=index.rst ../README.md

# add a toctree to link the other markdown files
cat >> index.rst << EOF

THESE PAGES ARE UNDER CONSTRUCTION AND RESTRUCTURING

Overview
========
Introduction to archival gravitational wave data and how rucio is implemented
as a solution to bulk data management.

.. toctree::
   :maxdepth: 3

   overview/data
   overview/policy
   overview/workflows
   overview/lfn2pfn
   overview/gwrucio

Installation & Setup
====================

.. toctree::
   :maxdepth: 2

   setup/server
   setup/database
   setup/client

Site Administrator Guide
========================
Notes on how to receive data from rucio at your computing site.

Rucio Administrator Guide
=========================

.. toctree::
   :maxdepth: 2

   admins/accounts
   admins/scopes
   admins/rses

User Guide
==========

.. toctree::
   :maxdepth: 2

   users/data-registration
   users/data-replication
   users/data-removal

Reference
=========

.. toctree::
   :maxdepth: 2

  daemons/daemons

Further Reading
===============

.. toctree::
   :maxdepth: 2

   references

EOF

# run sphinx
python3 -m sphinx -M html . _build

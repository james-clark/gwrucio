# LFN to PFN Algorithm
Gravitational wave frame files follow the naming convention
::

    <obs>-<frame_type>-<epoch>-<duration>.gwf

 * Observatory from which data is taken. E.g., H (Hanford), L (Livingston), V (Virgo), G (GEO600), K (KAGRA).
 * Frame type: description of instrument and data reduction level / calibration version.  E.g., `H1_HOFT_C00`.
 * Epoch: GPS start time of data in frame file.
 * Duration: number of seconds of data.

In rucio, the names of files, datasets and containers (collections of data
objects) are composed of two strings: the *scope* and a *name*. The combination
of both is called a data identifier (DID). For instance a file identifier
(logical filename, LFN) is composed of a scope and a file name like
`scope:name`.  For gravitational wave data we adopt the convention:
 * Scope: observing epoch.
 * Name: LFN of file

Example: the frame file `L-L1_HOFT_C01-1164513280-4096.gwf` (calibration
level-1 Livingston strain data taken during the 10th engineering run,
ER10), appears in rucio simply as `ER10:L-L1_HOFT_C01-1164513280-4096.gwf`.

The path to files on storage (physical filename, PFN) follows a deterministic
algorithm whereby the PFN is determined directly from the LFN and a
site-specific prefix.  

Note that some data types (e.g., Virgo data, raw frames and `C00` frame types)
have followed slightly different PFN conventions, requiring some exeptions to
be built into our [LFN2PFN
algorithm](https://git.ligo.org/james-clark/gwrucio/blob/master/gwrucio/lfn2pfn.py).

Examples of LFN->PFN conversion:


| **LFN** | **Epoch/Scope** | **PFN** | **DID** |
|:-----------------------------------:|:--------------:|:--------------:|:--------------:|
| `H-H1_HOFT_C01-1188003840-4096.gwf` | `O2`       | `/hdfs/frames/O2/hoft_C01/H1/H-H1_HOFT_C01-11880/H-H1_HOFT_C01-1188003840-4096.gwf`  | `O2:H-H1_HOFT_C01-1188003840-4096.gwf`    
| `L-L1_HOFT_C00-1158533120-4096.gwf` | `postO1`   | `/hdfs/frames/postO1/hoft/L1/L-L1_HOFT_C00-11585/L-L1_HOFT_C00-1158533120-4096.gwf`  | `postO1:L-L1_HOFT_C00-1158533120-4096.gwf`
| `V-V1Online-1192294000-2000.gwf`    | `AdVirgo`  | `/hdfs/frames/AdVirgo/HrecOnline/V-V1Online-11922/V-V1Online-1192294000-2000.gwf`    | `AdVirgo:V-V1Online-1192294000-2000.gwf`  
| `H-H1_R-1126631040-64.gwf`          | `O1`       | `/hdfs/frames/O1/raw/H1/H-H1_R-11266/H-H1_R-1126631040-64.gwf`                       | `O1::H-H1_R-1126631040-64.gwf`            

 * The LFN2PFN algorithm is implemented in [gwrucio/lfn2pfn.py](https://git.ligo.org/james-clark/gwrucio/blob/master/gwrucio/lfn2pfn.py).
 * Using this algorithm requires a server running [this fork](https://github.com/astroclark/rucio) of rucio.



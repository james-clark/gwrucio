# Gravitational Wave Data
Archival gravitational wave data is stored in frame files: containers with some
number of separate channels which each store some instrumental, environmental
or astrophysical observation (typically, but not limited to, some number of
seconds of time series observations).  A technical document describing the
frame file specification can be found [here](https://dcc.ligo.org/T970130).

Frame files come in different types at increasing levels of data reduction.
Raw frame files contain all instrumental channels.  As further calibration
measurements are made, these raw data sets are refined to produce frames which
contain downsampled and smaller numbers of auxiliary channels.  A final level
of data reduction is then applied to produce HOFT (as in h(t); “h-of-t”) frames
whose channels simply report the instrument state and the gravitational wave
strain itself and which comprise the input files to most offline gravitational
wave data analysis.

LIGO data sets then, are defined by:

 * Instrument: e.g., H1, L1 or V1
 * Frame type: raw, reduced-data-set (RDS) or HOFT which itself comes in 3
   levels of calibration refinement: C00, C01 and C02.
 * Observing epoch: ERX (engineering runs), OX (observing runs) and post OX
   (inter-run data)

The LSC & Virgo also produce Short Fourier transform files (SFTs) used for
continuous wave analyses.  The present discussion will refer explicitly to
frame files but SFTs follow similar conventions.


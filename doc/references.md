# References

## Useful Links
### Public
 * [Rucio documentation](http://rucio.readthedocs.io/)
 * [Rucio @ GitHub](https://github.com/rucio/)
 * [Rucio for GWs  @ GitHub (fork)](https://github.com/astroclark/rucio)
### LSC/Virgo/KAGRA authentication required
 * [LSC computing wiki](https://wiki.ligo.org/Computing/)
 * [LIGO Data Replicator (LDR)](https://wiki.ligo.org/Computing/DASWG/LIGODataReplicator)
 * [LIGO DCC search for `rucio`](https://dcc.ligo.org/cgi-bin/private/DocDB/Search?.submit=These+Words+&simpletext=rucio&simple=1&maxdocs=500)
## Contact
```
===========================================
James Alexander Clark
Research Scientist

Center for Relativistic Astrophysics
School of Physics
Georgia Institute of Technology
Atlanta GA 30332
email:  james.clark@ligo.org
Skype/hangouts: jclark.astro@gmail.com
===========================================
```


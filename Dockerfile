FROM centos:7
ARG version
MAINTAINER James Alexander Clark <james.clark@ligo.org>
RUN echo "Building gwrucio"

RUN rpm -ivh http://software.ligo.org/lscsoft/scientific/7/x86_64/production/l/lscsoft-production-config-1.3-1.el7.noarch.rpm && \
      rpm -ivh http://repo.opensciencegrid.org/osg/3.4/osg-3.4-el7-release-latest.rpm

RUN yum install -y epel-release.noarch && \
    yum upgrade -y 

RUN yum install -y \
      git \
      gcc \
      ldg-client \
      ldas-tools-diskcacheAPI \
      libaio \
      MySQL-python \
      openssl-devel \
      python-devel \
      python-pip \
      vim \
      vo-client \
      voms-clients-cpp \
      yum clean all && \
      rm -rf /var/cache/yum

RUN yum install -y \
      gfal2-all \
      gfal2-python \
      gfal2-util \
      yum clean all && \
      rm -rf /var/cache/yum


RUN pip install --upgrade pip setuptools
RUN rm -rf /usr/lib/python2.7/site-packages/ipaddress*
RUN pip install --no-cache-dir \
      lalsuite \
      lscsoft-glue \
      psycopg2-binary \
      PyYAML \
      rucio[postgresql]

RUN pip install --no-cache-dir \
      git+https://git.ligo.org/james-clark/gwrucio.git#egg=gwrucio \
      git+https://git.ligo.org/rucio/igwn-rucio-lfn2pfn.git#egg=igwn-rucio-lfn2pfn

# Directory setup for rucio configuration and data directory bindings
RUN mkdir -p /opt/rucio/etc /archive /net /hdfs /local

# --------- Customisation & Conveniences --------- #
COPY entrypoint/ps1.sh /etc/profile.d/
COPY entrypoint/inputrc /etc/inputrc
COPY entrypoint/bashrc /root/.bashrc
RUN echo 'eval "$(register-python-argcomplete rucio)"' >> /root/.bashrc
RUN echo 'eval "$(register-python-argcomplete rucio-admin)"' >> /root/.bashrc

# COPY /entrypoint/startup /usr/local/bin/startup
# WORKDIR /container/gwrucio
# RUN useradd -m -d /container/gwrucio -s /bin/bash gwrucio
# USER gwrucio
# ENTRYPOINT [ "/usr/local/bin/startup" ]
# CMD ["/bin/bash", "-l" ]

This directory contains some example wrapper scripts for running the
containerised `gwrucio_registrar`

 * `gwrucio_docker_shell`: places user in an docker container environment with
   access to all rucio-related software.
 * `gwrucio_singularity_shell`: places user in an singularity container environment with
   access to all rucio-related software.
 * `gwrucio_registrar_wrapper`: calls `gwrucio_registrar` from a singularity
   container.

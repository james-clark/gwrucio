#!/usr/bin/env python
# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Plot GridFTP transfer rates and statistics
"""

#import sys
import argparse
import datetime
import collections
#import numpy
#from matplotlib import pyplot

def parse_cmdline():
    """Parse command line"""

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("logfiles", type=str, default=None, nargs='+',
                        help="""GridFTP log files""")
    parser.add_argument("--ifo", type=str, default="H1",
                        help="""Instrument to analyze""")
    parser.add_argument("--keep-lfns", type=str, default=None,
                        help="""List of LFNs to parse out of the log""")

    cmdline_args = parser.parse_args()

    return cmdline_args

class TransferLog(object):
    """
    GridFTP transfer statistics for a list of LFNs in a gridftp log
    """
    def __init__(self, log_files, keep_lfns_file=None):
        """
        Initialize
        """
        # Parse log files
        self.parse_logs(log_files)

        if keep_lfns_file is not None:
            with open(keep_lfns_file, 'r') as lfns:
                keep_lfns = lfns.read().splitlines()
            self.trim_logs(keep_lfns)

    def trim_logs(self, keep_lfns):
        """
        Trim down to logs just for these files
        """
        for pfn in self.logdata['file']:
            lfn = pfn.split('/')[-1]
            if lfn not in keep_lfns:
                removeidx = self.logdata['file'].index(pfn)
                for key in self.logdata.keys():
                    self.logdata[key].pop(removeidx)

    def parse_logs(self, log_files):
        """
        Parse GridFTP logs
        """

        self.logdata = collections.defaultdict(list)

        for log_file in log_files:

            with open(log_file, 'r') as lof:
                this_data = lof.read().splitlines()

            for line in this_data:

                for expr in line.split():
                    var = expr.split('=')[0]
                    val = expr.split('=')[1]

                    if var in ['ts', 'end']:
                        val = convert_time(val)

                    self.logdata[var].append(val)


#   def throughput(self, filename, log):
#       """
#       Compute the throughput (mb/s and nbytes for a given file)
#       """
#
#       mbs = (log['nbytes']/1024.)/log['dur']
#       return mbs, log['nbytes']/1024.

def convert_time(datestr):
    """
    Create datetime object from time stamp
    """

    date = [int(dnum) for dnum in datestr.split('T')[0].split('-')]
    time = [int(tim) for tim in datestr.split('T')[1].split('.')[0].split(':')]
    milli = [int(datestr.split('T')[1].split('.')[1].replace('Z', ''))]

    return datetime.datetime(*(date+time+milli))


#########################################################################

def main():
    """
    Main
    """

    ## Parse input
    args = parse_cmdline()

    ## Parse log files
    log = TransferLog(args.logfiles, args.keep_lfns)

    return log


if __name__ == "__main__":

    LOG = main()
